const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productController");
const auth = require("../middleware/auth");
const checkAdminRole = require('../middleware/checkAdminRole');
const jwt = require("jsonwebtoken");

const {
  getAllProducts,
  getProduct,
  addProduct,
  updateProduct,
  getAllActive,
  deleteProduct,
} = productControllers

const readToken = async (req, res, next) => {
  let token = req.headers.authorization;
  console.log(req.headers);
  console.log(token);
  if (!token) return next();
  try {
    const secret = "CourseBookingAPI";
    token = token.slice(7, token.length);
    req.user = jwt.verify(token, secret);
    return next();
  } catch (e) {
    next();
  }
}
router.get('/', readToken, getAllProducts);
router.get('/listproduct', getAllActive);
router.get("/:id", getProduct);

router.use(auth.verify);

router.use(checkAdminRole);
// router.get("/", getAllProducts);
router.post("/", addProduct)
router.route('/:id')
  .put(updateProduct)
  .delete(deleteProduct)

module.exports = router;

