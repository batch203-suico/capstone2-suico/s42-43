const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../middleware/auth");
const checkAdminRole = require('../middleware/checkAdminRole');

const {
  registerUser,
  loginUser,
  setAsAdmin,
  userDetails,
} = userControllers

console.log(userControllers);

/* Customer Routes */
router.post("/register", registerUser); // 2.
router.post("/login", loginUser); // 1. 

router.use(auth.verify);
router.get("/user-details", userDetails);

/* Admin Routes */
router.patch("/:id/setAsAdmin", checkAdminRole, setAsAdmin); //3.
router.get("/orders");

module.exports = router;


/* 1. Application level middleware app.use
2. Route level middleware (router.put('/')) / inline middle
3. Router Level router.use(); */