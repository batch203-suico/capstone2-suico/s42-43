const router = require("express").Router();
const auth = require("../middleware/auth");
const orderController = require('../controllers/orderController');


router.use(auth.verify);
router.get("/", orderController.getOrders);
router.get('/:orderId', orderController.getOrderById);
router.post('/checkout', orderController.createOrder);

module.exports = router;