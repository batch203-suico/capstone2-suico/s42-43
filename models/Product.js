const mongoose = require("mongoose");
const refObjectId = mongoose.Schema.Types.ObjectId;

const productSchema = new mongoose.Schema({
	name: { type: String, required: true },
	description: { type: String, required: true },
	owner: { type: String, required: true },
	price: { type: Number, required: true },
	isActive: { type: Boolean, default: true },
	stock: { type: Number, required: true, default: 0 },
	createdOn: {  type: Date, default: new Date() },
	orders: [{
		type: refObjectId, ref: 'Order'
	}]

},
	{ timestamps: true }
);


module.exports = mongoose.model("Product", productSchema);
