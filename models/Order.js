const mongoose = require("mongoose");
const refObjectId = mongoose.Schema.Types.ObjectId;

const orderSchema = new mongoose.Schema({
	user: { type: refObjectId, ref: 'User', required: true },
	products: [{
		productId: { type: refObjectId, ref: 'Product', required: true },
		quantity: { type: Number, default: 1 }
	}],
	totalAmount: { type: Number },
	shippingAddress: { type: String },
	purchasedOn: { type: Date, default: new Date() },
},
	{ timestamps: true }
);

module.exports = mongoose.model("Order", orderSchema);

