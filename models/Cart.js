const mongoose = require("mongoose");
const Product = require("./Product");
const User = require("./User");
const refObjectId = mongoose.Schema.Types.ObjectId;

const cartSchema = new mongoose.Schema({
        userId: { type: refObjectId, required: true },
        products: [{
                productId: {
                        type: refObjectId,
                        ref: 'Product', required: true
                },
                quantity: { type: Number, default: 1 }
        }]

})

module.exports = mongoose.model("Cart", cartSchema);
