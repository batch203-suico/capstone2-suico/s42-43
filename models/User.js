const mongoose = require("mongoose");
const refObjectId = mongoose.Schema.Types.ObjectId;

const userSchema = new mongoose.Schema({
	username: { type: String, required: true, unique: true },
	email: { type: String, required: true, unique: true },
	password: { type: String, required: true },
	mobileNo: { type: String, required: true },
	isAdmin: { type: Boolean, default: false },
	orders: [{
		type: refObjectId, ref: 'Order'
	}]
},
	{ timestamps: true }
);

module.exports = mongoose.model("User", userSchema);
