const checkAdminRole = (req, res, next) => {
  if (req.user.isAdmin) next();
  else res.status(401).send("You don't have access to this page!");
}

module.exports = checkAdminRole;