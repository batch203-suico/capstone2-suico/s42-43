const jwt = require("jsonwebtoken");
const { getProfile } = require('../controllers/userControllers');

const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}


// Middleware function
module.exports.verify = (req, res, next) => {
	console.log('Authentication running...');
	let token = req.headers.authorization;
	console.log(req.headers);
	if (!token) return res.status(401).send({ message: "Auth failed. No token provided!" });
	token = token.slice(7, token.length);
	console.log(token);
	try {
		const data = jwt.verify(token, secret);
		if (data) req.user = data;
		return next();
	} catch (e) {
		return res.status(400).send('Invalid token!');
	}
}

// Token decryption
/*
	- Analogy
		Open the gift and get the content
*/
module.exports.decode = (token) => {
	if (token !== undefined) {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				// If token is not valid
				return null;
			}
			else {
				// decode method is used to obtain the information form the JWT
				// Syntax: jwt.decode(token, [options]);
				// Returns an object with access to the "payload" property which contains the user information stored when the token is generated.
				return jwt.decode(token, { complete: true }).payload;
			}
		})
	}
	else {
		//If token does not exist
		return null
	}
}
