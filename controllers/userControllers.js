const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../middleware/auth");

const registerUser = async (req, res) => {
  try {
    const SALT_ROUNDS = 10;
    const newUser = new User({
      username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, SALT_ROUNDS),
      mobileNo: req.body.mobileNo,

    })
    const registeredUser = await newUser.save()
    res.status(201).send(registeredUser)
  } catch (error) {
    res.status(500).send(error.message)
  }
}


const loginUser = async (req, res) => {
  const enteredPassword = req.body.password;
  try {
    const retrievedUser = await User.findOne({ email: req.body.email });
    if (!retrievedUser) return res.status(404).send({ message: 'No User found. ' });
    if (!retrievedUser) console.log('aw')
    const userPwOnDb = retrievedUser.password;

    const isPasswordCorrect = bcrypt.compareSync(enteredPassword, userPwOnDb);
    if (isPasswordCorrect) {
      const response = {
        accessToken: auth.createAccessToken(retrievedUser),
        userDetails: { ...retrievedUser._doc },
      }
      delete response.userDetails.password
      return res.send(response)
    }
    res.status(403).send({ message: "Incorrect password!" })
  }
  catch (error) {
    console.log(error.stack)
    res.status(500).send(error.message)
  }
}

const userDetails = async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
		console.log(userData);
		const result = await User.findById(userData.id)
		res.send(result);
		}
  catch (error) {
    res.status(500).send(error.message)
  }
}

const setAsAdmin = async (req, res) => {
  try {
    const payload = {isAdmin: true};
    await User.findByIdAndUpdate(req.params.id, payload);
    res.status(200).send(payload);
  }
  catch (error) {
    res.status(500).send(error.message)
  }
}

const checkOut = async (req, res) => {
  const newOrder = await new Order({
    user: req.body.user,
    product: req.body.product,
    shippingAddress: req.body.shippingAddress,
  }).save()
  const payload = {
    $push: {orders: newOrder._id}
  }
  await Product.findByIdAndUpdate(req.body.product.productId, payload)
  await User.findByIdAndUpdate(req.body.user, payload)

  

}

module.exports= {
  registerUser,
  loginUser,
  userDetails,
  setAsAdmin,
  checkOut,
};