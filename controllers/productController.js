const Product = require("../models/Product");
const auth = require("../middleware/auth");

const addProduct = async (req, res) => {
  console.log('req.user', req.user);
  const productDraft = {
    name: req.body.name,
    description: req.body.description,
    owner: req.body.owner,
    price: req.body.price,
    isActive: req.body.isActive,
    createdOn: req.body.createdOn,
    stock: req.body.stock,
  }
  try {
    let newProduct = new Product(productDraft)
    const newProductAdded = await newProduct.save()
    res.status(201).send(newProductAdded);
  } catch (error) {
    res.status(500).send(false);
  }
}
const getAllProducts = async (req, res) => {
  const filter = {};
  const { activeOnly, inactiveOnly } = req.query;
  if (!req.user?.isAdmin) filter.isActive = true;
  else if (req.user?.isAdmin && activeOnly) filter.isActive = true;
  else if (req.user?.isAdmin && inactiveOnly) filter.isActive = false;
  try {
    const allProduct = await Product.find(filter);
    console.log(filter)
    res.send(allProduct);
  } catch (error) {
    console.log(error.message);
  }
}
const getProduct = async (req, res) => {
  console.log(req.params.productId);

  const product = await Product.findById(req.params.id)
  res.send(product);
}


const updateProduct = async (req, res) => {
  try {
    let updateProduct = {
      name: req.body.name,
      description: req.body.description,
      owner: req.body.owner,
      price: req.body.price,
      stock: req.body.stock,
      isActive: req.body.isActive,
    }
    Object.keys(updateProduct).forEach((key) => {
      if (updateProduct[key] === undefined) delete updateProduct[key];
    });
    const product = await Product.findByIdAndUpdate(req.params.id, updateProduct, { new: true })
    res.status(201).send(product);
  } catch (error) {
    res.status(500).send(false);
  }

}

const getAllActive = async (req, res) => {
  const result = await Product.find({ isActive: true })
  res.send(result);
}

const deleteProduct = async (req, res) => {
  const { id: productId } = req.params;
  if (!productId) return re.status(400).send('Product ID is required.');
  await Product.findByIdAndDelete(productId);
  return res.status(204).send();
}

module.exports = {
  addProduct,
  getAllProducts,
  getProduct,
  updateProduct,
  getAllActive,
  deleteProduct,
};

