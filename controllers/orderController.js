const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");
// const auth = require("../midd

/* 
const checkOut = async (req, res) => {
  const newOrder = await new Order({
    user: req.body.user,
    product: req.body.product,
    shippingAddress: req.body.shippingAddress,
  }).save()
  const payload = {
    $push: {orders: newOrder._id}
  }
  await Product.findByIdAndUpdate(req.body.product.productId, payload)
  await User.findByIdAndUpdate(req.body.user, payload)

*/
const createOrder = async (req, res) => {
  // 1. Create Order draft, create new order.
  const {
    products,
    totalAmount,
  } = req.body;
  const userId = req.user.id;
  const orderDraft = {
    user: userId,
    products,
    totalAmount,
  }
  console.log(orderDraft)

  // Use the ordel model to create the order draft
  const newOrder = await new Order(orderDraft).save();
  // Find the existing user using the order draft
  const retrievedUser = await User.findById(newOrder.user);

  // 2. Retrieve User, push order to users' orders array then save.
  retrievedUser.orders.push(newOrder._id);
  await retrievedUser.save();

  // 3. Retrieve Product, push order to product's order array, then save.
  for (item of newOrder.products) {
    const retrievedProduct = await Product.findById(item.productId);
    retrievedProduct.orders.push(newOrder._id);
    retrievedProduct.stock -= item.quantity;
    await retrievedProduct.save();
  }

  res.status(201).send(newOrder);
}

const getOrders = async (req, res) => {
  const filter = {};
  if (!req.user.isAdmin) filter.user = req.user.id;

  const retrievedOrders = await Order.find(filter).populate({ path: 'user', select: 'email' }).exec();
  const curatedOrders = retrievedOrders.map(({ _doc }) => _doc);
  return res.status(200).send({ orders: curatedOrders })
}

const getOrderById = async (req, res) => {
  const orderId = req.params.orderId;
  if (!req.user) return res.status(403).send();
  if (!orderId) return res.status(400).send();

  const retrievedOrder = await Order
    .findById(orderId)
  return res.status(200).send(retrievedOrder);
}

module.exports = {
  createOrder,
  getOrders,
  getOrderById,
}